There is a Sigma Intership Test Task. It is a website with two accessible pages:

-history_rates/. On this page user will see a graph of changes in the currency rate UAH to USD or an empty database notification.

If user want to add currency rate, he shoud click on the button under the graph.

-add_rate/. On this page user will see a form to create and save currency rate with two fields: date and value of currency rate.  All the fields are mantadory.

After filling out the form user should click on the button "Save currency rate". Server will check the following conclusions:

  -Value of currency rate is a float or an integer number.
  -Value of currency rate is a positive number.
  -Date of currency rate follows a specific format.

If a mistake was made, user will see the corresponding message. 

If everything is filled according to the rules, dataabse will save the currency rate and user will be redirected to page with graph.